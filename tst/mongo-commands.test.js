var MongoClient = require('mongodb').MongoClient



describe('Src :: MongoCommands', () => {
  let db;
  beforeAll((done) => {
    const url = 'mongodb://admin:admin@localhost:27017';
    MongoClient.connect(url, function (err, connectedDb) {
      if (err) {
        throw err
      }
      db = connectedDb;
      // db.dropDatabase();
      const dataImport = require('../src/data-import');
      dataImport(db, function() {
        done();
      });
    });
  });

  afterEach(() => {
    db.collection('coursereport').remove();
  })

  test('should contain 5 records', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.find().toArray();
      expect(result).toBeDefined();
      expect(result.length).toBe(5);
      done()
    })
  }, 50000);
  test('should contain correct course number for Jeff', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'jeff'});
      expect(result.value.numbercourses).toEqual(2)
      done()
    })
  }, 50000);
  test('should contain correct name for Jeff', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'jeff'})
      expect(result.value.name).toEqual('Jeff Holland');
      done()
    })
  }, 50000);
  test('should contain correct course number for john.shore', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'john.shore'})
      expect(result.value.numbercourses).toEqual(2)
      done()
    })
  }, 50000);
  test('should contain correct name for john.shore', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'john.shore'})
      expect(result.value.name).toEqual('John Shore')
      done()
    })
  }, 50000);
  test('should contain correct course number for scott', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'scott'})
      expect(result.value.numbercourses).toEqual(3)
      done()
    })
  }, 50000);
  test('should contain correct name for john.shore', async (done) => {
    const produceReport = require('../src/mongo-commands');
    const coursereports = await produceReport(db);
    Promise.all(coursereports).then(async coursereport => {
      report = db.collection('coursereport')
      await report.insertMany(coursereport);
      const result = await report.findOne({_id: 'scott'})
      expect(result.value.name).toEqual('Scott Mills')
      done()
    })
  }, 50000);
});


