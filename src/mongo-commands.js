async function produceReport(db) {
  const students = await db.collection('students').find({}).toArray();
  const reports = students.map(async (student) => {
    const courses = await db.collection('courses').find({ students: student._id }).toArray();
    return Promise.resolve({
      _id: student._id,
      value: {
        name: `${student.name.first} ${student.name.last}`,
        numbercourses: courses.length
      }
    });
  })

  // Promise.all(reports).then(report => console.log(report));
  return reports
}

module.exports = produceReport;
// coursereport
